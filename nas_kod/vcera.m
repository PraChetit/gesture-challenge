N = read_movie(sprintf('%s/devel%02d/K_%d.avi', data_dir, 10, 12));
K = read_movie(sprintf('%s/devel%02d/M_%d.avi', data_dir, 10, 12));
len = length(N);
M = N;
for i = 1:len, M(i).cdata = rgb2gray(N(i).cdata);end
Max = M(1).cdata;
for i = 1:len, Max = max(Max, M(i).cdata); end
for i = 1:len, I = M(i).cdata; I(I<10) = 255; I(Max - I < 10) = 255; M(i).cdata = I; end
for i = 1:len, I = M(i).cdata == 255; I=bwdist(I)>=3; I=bwdist(I)<=3; M(i).cdata(~I)=255;end

k = [50,50;205,57;176,94;113,151];
m = [67,63;208,63;175,101;123,153];
Fx=[k(:,1), ones(4,1)]; Yx=m(:,1);
Fy=[k(:,2), ones(4,1)]; Yy=m(:,2);
x = ((Fx' * Fx)^(-1))*(Fx')*Yx;
y = ((Fy' * Fy)^(-1))*(Fy')*Yy;
sx = round(320*x(1)); sy = round(240*y(1)); tx = round(x(2)); ty = round(y(2));

for i = 1:len
    I = imresize(M(i).cdata, [sy, sx]) < 255;
    I = [false(ty-1, 320); false(sy, tx-1), I, false(sy, 320-sx-tx+1); false(240-sy-ty+1, 320)];
    for j = 1:3
        J = K(i).cdata(:,:,j);
        J(~I) = 255;
        N(i).cdata(:,:,j) = J;
    end
end