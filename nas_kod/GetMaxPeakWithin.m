% first lame algorithm for finding resting position
% simply average first frames with background removed in train set of videos

my_data = GetCurrentPath();
my_data = [my_data '\Data'];

maxPeakWithin = cell(480, 1);
% set i for the batches you want to rock on
parfor i = 1:480
    path = sprintf('%s\\devel%02d', my_data, i);
    train = read_file(sprintf('%s\\devel%02d_train.csv', path, i));
    len = length(train);
    RP = imread(sprintf('%s\\AvgFirstFrame.bmp', path), 'bmp');
    
    maxPeakWithin{i} = 0;
    for j = 1:len
        K = read_movie(sprintf('%s\\K_%d.avi', path, j));
        [i0, in] = trim_video(K);
        K = K(i0:in);
        peakWithin = GetLargestPeakWithin(RP, K);
        if peakWithin > maxPeakWithin{i}
            maxPeakWithin{i} = peakWithin;
        end
        
    end
    if maxPeakWithin{i} < 0.15
        maxPeakWithin{i} = 0.25;
    elseif maxPeakWithin{i} >= 0.15 && maxPeakWithin{i} < 0.65
        maxPeakWithin{i} = maxPeakWithin{i} + 0.1;
    else
        maxPeakWithin{i} = 0.75;
    end
    
    disp(i);
end

for i = 1:480
    m = maxPeakWithin{i};
    save(sprintf('%s\\devel%02d\\maxPeakWithin.mat', my_data, i), 'm');
end
