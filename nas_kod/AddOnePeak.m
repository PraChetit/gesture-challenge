function [bool] = AddOnePeak(widthMax, n, data, avgLen)
    % decides, whether to add a peak to list of peaks
    % widthMax, data, avgLen are the same as in FindMostRelevantPeaks.m
    % n = # of peak in widthMax, which is our candidate
    % 
    % returns true, if the candidate should be added
    
    bool = false;

    if widthMax(n, 2) > avgLen / 2.75
        higher = length(data);
        lower = 1;
        % find nearest neighbours of the candidate
        for i = 1:n-1
            if (widthMax(i, 1) > lower) && (widthMax(i, 1) < widthMax(n))
                lower = widthMax(i, 1);
            end
            if (widthMax(i, 1) < higher) && (widthMax(i, 1) > widthMax(n))
                higher = widthMax(i, 1);
            end
        end
        
        % find the minimums between neghbours and candidate
        minLo = 1; % constant, needs to be changed, if normalization is left behind
        minHi = 1; % constant, needs to be changed, if normalization is left behind
        for i = lower:widthMax(n, 1)
            if data(i) < minLo
                minLo = data(i);
            end
        end
        for i = widthMax(n, 1):higher
            if data(i) < minHi
                minHi = data(i);
            end
        end
        
        % if the candidate is high enough
        % this is pretty creepy, has to be enhanced
        if widthMax(n, 3) >= (data(lower) + minLo) / 2 && ...
                widthMax(n, 3) >= (data(higher) + minHi) / 2
            bool = true;
        end
    end
    
end