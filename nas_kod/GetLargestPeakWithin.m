function [size] = GetLargestPeakWithin(RP, movie)
    
    if nargin == 2
        data = ComputeFrameCorrelations(RP, movie);
    else
        data = RP;
    end
    len = length(data);
    widthMax = ones(len, 3);
    % for each peak compute how wide peak it is
    for i = 1:len
        k = 1;
        while true
            if i - k > 0
                if data(i) < data(i-k); break; end
            end
            if i + k < len + 1
                if data(i) < data(i+k); break; end
            end
            k = k + 1;
            if (i - k < 0) && (i + k > len + 1); break; end
        end
        widthMax(i, :) = [i, k, data(i)];
    end
    
    % sort the points according to its widths
    widthMax = -sortrows(-widthMax, 2);
    
    a = min(widthMax(1, 1), widthMax(2, 1));
    b = max(widthMax(1, 1), widthMax(2, 1));
    
    fifth = round((b-a) / 5);
    vid = (a + fifth):(b - fifth);
    widthMax = sortrows(widthMax, 1);
    widthMax = widthMax(vid, :);
    widthMax = -sortrows(-widthMax, 2);
    
    k = 1;
    while true
        if widthMax(k, 2) < 6; break; end
        k = k + 1;
    end
    if k == 1
        size = 0;
        return;
    end
    
    widthMax = -sortrows(-widthMax(1:k-1, :), 3);
    peak = widthMax(1, 1);
    
    m = min(data(a:peak));
    s1 = (data(peak) - m) / (data(a) - m);
    m = min(data(peak:b));
    s2 = (data(peak) - m) / (data(b) - m);
    size = max(s1, s2);
    
end