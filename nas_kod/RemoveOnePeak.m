function [number] = RemoveOnePeak(widthMax, n, data, avgLen)
    % decides, whether to remove one peak, and which
    % widthMax, data, avgLen are the same as in FindMostRelevantPeaks.m
    % n = count of peaks already active
    % 
    % returns 0, if no peak should be removed, otherwise returns its number
    % in widthMax.
    
    number = 0;
    
    % if the width of last peak is too narrow, kill it
    if widthMax(n, 2) < avgLen / 3.1
        number = n;
        return;
    end
    
    % if the peak isn't very high, delete it
    widthMax = widthMax(1:n, :);
    widthMax(:, 4) = [1:n]'; 
    w = -sortrows(-widthMax(1:n, :), 3);
    if w(n, 3) < w(n-1, 3) / 2
        number = w(n, 4);
        return;
    end
    
%     number = 0;
%     
%     % if the width of last peak is too narrow, kill it
%     if widthMax(n, 2) < avgLen / 3.5
%         number = n;
%         return;
%     end
%     
%     % if the peak isn't very high, delete it
%     widthMax = widthMax(1:n, :);
%     a = 1:n;
%     widthMax(:, 4) = a';
%     w = sortrows(widthMax(1:n, :), 1);
%     k = a(w(:,4) == n);
%     
%     minLo = 1; % constant, needs to be changed, if normalization is left behind
%     minHi = 1; % constant, needs to be changed, if normalization is left behind
%     
%     if k ~= 1
%         for i = w(k-1, 1):w(k, 1)
%             if data(i) < minLo
%                 minLo = data(i);
%             end
%         end
%         if w(k, 3) < (w(k-1, 3) + minLo) / 2
%             number = n;
%             return;
%         end
%     end
%     if k ~= n
%         for i = w(k, 1):w(k+1, 1)
%             if data(i) < minHi
%                 minHi = data(i);
%             end
%         end
%         if w(k, 3) < (w(k+1, 3) + minHi) / 2
%             number = n;
%             return;
%         end
%     end
%     
end