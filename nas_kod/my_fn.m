function f = my_fn(a, b, c, s)
    
    x = ones(s, 1) * ((1:s) - (s+1)/2);
    y = ((s+1)/2 - (1:s))' * ones(1, s);
    
    xn = cos(a) * x - sin(a) * y;
    yn = cos(a) * x + sin(a) * y;
    
    f = exp(-(xn.^2 + yn.^2) ./ (2*b^2)) .* cos(2*pi*c*xn) / (2*pi*b^2);
end