function L = motion_mask(M, edge_size)
    
    len = length(M);
    
    x = 240 / edge_size;
    y = 320 / edge_size;
    A = zeros(x, y);
    L = zeros(numel(A), len);
    
    A = kron(eye(x), ones(1, edge_size));
    B = kron(eye(y), ones(1, edge_size))';
    
    for k=2:length(M)
        D = M(k).cdata(:,:,1) - M(k-1).cdata(:,:,1);
        D = D > 10;
        C = A * D * B;
        L(:,k) = C(:);
    end
    
    L = L';
end