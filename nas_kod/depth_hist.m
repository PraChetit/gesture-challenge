function res = depth_hist(M, param, movie_type)
    
    len = length(M);
    
    h = 240/param(1);
    w = 320/param(1);
    cells = h*w;
    W = kron(reshape(1:cells, h, w), ones(param(1)));
    
    for i = 1:len
        M(i).cdata = rgb2gray(M(i).cdata);
    end
    
    Max = M(1).cdata;
    for i = 2:len
        Max = max(Max, M(i).cdata);
    end
    Max = double(Max);
    
    for i=1:len
        I = M(i).cdata;
        I = double(I);
        I(I< 10) = -1000;
        I((Max - I) < 10) = 0;
        
        V = round(I * ((param(2)-1) / max(Max(:))));
        D = ones(240, 320);
        D(V < 0) = 0;
        
        % NORMALIZING
        s = accumarray(W(:), D(:));
        s = ones(param(2), 1) * s';
        s(s == 0) = param(2);
        
        D(V == 0) = 0;
        V(V <= 0) = 0;
        V = V + 1;
        
        t = accumarray([V(:), W(:)], D(:));
        
        if size(t, 1) < param(2)
            t = [t; zeros(param(2) - size(t,1), cells)];
        end
        
        % NORMALIZING
        t(:) = t(:) ./ s(:); % /cells
        
        % JOINING 2x2
%         B = [zeros(1, h-1); eye(h-1)] + [eye(h-1); zeros(1, h-1)];
%         C = [zeros(1, w-1); eye(w-1)] + [eye(w-1); zeros(1, w-1)];
%         A = sparse(kron(C, B));
%         t = t * A;
        
        res(i, :) = t(:) / sum(t(:));
    end
end