function res = video_hog_diff(K, param)
    
    len = length(K);
    res = [];
    
    for i = 1:len
        K(i).cdata = double(rgb2gray(K(i).cdata));
    end
    for i = 1:len-1
        K(i).cdata = medfilt2(K(i+1).cdata - K(i).cdata, [7, 7]);
    end
    
    for i = 2:len
        temp = hog(K(i-1).cdata, param(1), param(2));
        temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
        temp = permute(temp, [3, 1, 2]);
        res(i-1,:) = temp(:)./sum(temp(:));
    end
    
end