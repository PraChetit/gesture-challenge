function res = video_hog_hof(M, param, K)
    
    len = length(K);
    res = cell(1, 2);
    
    for i = 2:len
        temp = hog(medfilt2(double(K(i).cdata(:,:,1)), [7, 7]), param(1), param(2));
        temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
        temp = permute(temp, [3, 1, 2]);
        res{1}(i-1,:) = temp(:)./sum(temp(:));
    end
    
    res{2} = video_hoof(M, param(4), param(3));
    
end