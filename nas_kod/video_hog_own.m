function res = video_hog_own(M, param)
    
    x = 240 / param(1);
    y = 320 / param(1);
    z = param(3) - 1;
    A = [eye(x - z), zeros(x - z, z)];
    B = [eye(y - z), zeros(y - z, z)];
    for i = 1:z
        A = A + [zeros(x-z, i), eye(x-z), zeros(x-z, z-i)];
        B = B + [zeros(y-z, i), eye(y-z), zeros(y-z, z-i)];
    end
    A = kron(A, ones(1, param(1)));
    B = kron(B, ones(1, param(1)))';
    
    res = zeros(length(M), (x-z)*(y-z)*param(2));

    for i = 1:param(2)
        f{i} = gabor_fn(1, i*pi/param(2), 3, 0, 0.5);
%         f{i} = my_fn(i*pi/param(2), 1.4, 0.35, 9);
    end
        
    for i = 1:length(M)
        temp = zeros(x - z, y - z, param(2));
        temp2 = rgb2gray(M(i).cdata);
        temp2 = bgremove(temp2);
        temp2 = medfilt2(temp2, [7 7], 'symmetric');
        for j = 1:param(2)
            temp3 = imfilter(temp2, f{j}, 'replicate');
            temp3 = imfilter(temp3, [-1,-1,-1;-1,8,-1;-1,-1,-1], 'replicate');
            temp3 = double(temp3);
            temp(:,:,j) = A * temp3 * B;
        end
        temp = permute(temp, [3, 1, 2]);
        res(i, :) = temp(:) / sum(temp(:));
    end
    
end