function [motionHistory] = ShowLowerPartMotion(K, batch, video, show)
    % K - 1 for depth video, 0 for RGB
    % batch - # of batch to load
    % video - # of video to load
    % show - if you want to show the plot, set to 1
    
    % set show to 0 if not provided
    if nargin < 4
        show = 0;
    end
    
    % path to the data
    my_data = GetCurrentPath();
    my_data = [my_data '\Data'];

    N = 480; % number of batches
    if ((batch < 1) || (batch > N)); return; end % quit if wrong params
    V = 47; % number of  videos in each batch
    if ((video < 1) || (video > V)); return; end % quit if wrong params
    
    % process inputs. path is the physical path to the videos
    if batch < 10
        path = sprintf('%s\\devel0%s', my_data, int2str(batch));
    else
        path = sprintf('%s\\devel%s', my_data, int2str(batch));
    end
    
    if K == 1
        path = sprintf('%s\\K_%s.avi', path, int2str(video));
    else
        path = sprintf('%s\\M_%s.avi', path, int2str(video));
    end
    
    % read movie and get the motion
    movie = read_movie(path);
    motionHistory = ComputeLowerPartMotion(movie);
    
    motionHistory = motionHistory / max(motionHistory(:));
    motionHistory = motionHistory(:) .^ 2;
    
    % plot the motion
    if show == 1
        plot(motionHistory);
    end

end