% first lame algorithm for finding resting position
% simply average first frames with background removed in train set of videos

my_data = GetCurrentPath();
my_data = [my_data '\Data'];

% set i for the batches you want to rock on
L = cell(480, 1);
parfor i = 1:480
    path = sprintf('%s\\devel%02d', my_data, i);
    train = read_file(sprintf('%s\\devel%02d_train.csv', path, i));
    len = length(train);
    
    L{i} = 0;
%     RPK = zeros(240, 320, 3);
    for j = 1:len
        K = read_movie(sprintf('%s\\K_%d.avi', path, j));
        [~, ~, frames] = trim_video(K);
        L{i} = L{i} + length(frames);
%         RPK = RPK + double(bgremove(K(1).cdata));
    end
    L{i} = round(L{i}/len);
    
%     
%     RPK = uint8(RPK ./ (len));
%     
%     RPK = imresize(RPK, 0.5);
%     imwrite(RPK, sprintf('%s\\avgFirstFrameSmall.bmp', path), 'bmp');
    
    disp(i);
end

for i = 1:480
    path = sprintf('%s\\devel%02d', my_data, i);
    Ltrim = L{i};
    save(sprintf('%s\\avgTrainLengthTrim.mat', path), 'Ltrim');
end