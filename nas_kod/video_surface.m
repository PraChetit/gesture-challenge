function [surface surfone] = video_surface(K, param, movie_type)
% 

len = length(K);
surface = zeros(len, 320*240);
surfone = false(len, 320*240);

gaussmoothing = fspecial('gaussian', [7, 7], 5);

for i=1:len
    temp = K(i).cdata(:,:,1);
    [temp back] = bgremove(temp);                 %
    temp = medfilt2(temp, [7 7], 'symmetric');
    temp = imresize(temp, 0.125);                 %
    temp = double(temp);                          %
%     ma = max(temp(temp~=back));                 %
    mi = min(temp(:));                            %
    temp = round((temp-mi)/(back-mi)*63);         %
    temp(temp > 63) = -1;                         %
    temp = temp * 1200;                           %
    temp = temp(:) + (1:1200)';                   %
    temp = temp(temp>0);                          %
%     temp = imfilter(temp, gaussmoothing, 'replicate');      %
%     temp = edge(temp);                                      %
    surfone(i,temp(:)) = true;
    temp = reshape(surfone(i,:), 30, 40, 64);     %
    temp = bwdist(temp);
    surface(i,:) = temp(:);
end

end