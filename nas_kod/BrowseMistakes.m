% Browsing videos with mistakes in length

h=figure('Name', 'Mistakes browser');

% load mistake matrix
mistakePlaces=load('mistakePlaces');
mistakePlaces=mistakePlaces.mistakePlaces;
% mistakePlaces=load('A');
% mistakePlaces=mistakePlaces.A;

s=1;
r=1;
u=[];
misindex=1;
misnum=1;
for i=1:5,
    for j=1:5,
        u(5*(i-1)+j)=uicontrol('Parent', h, 'Position', [30*j-30, 400-30*i, 30, 30],'BackgroundColor', [1 1 1], 'ForegroundColor', [0 0 0], 'String', size(mistakePlaces{i,j},1), 'Callback', sprintf('set(u(5*(r-1)+s), ''BackgroundColor'', [1 1 1], ''ForegroundColor'', [0 0 0]); r=%d; s=%d; set(u(%d), ''BackgroundColor'', [0 0 0], ''ForegroundColor'', [1 1 1]); misindex=1; misnum=size(mistakePlaces{r,s},1); set(u3,''String'', [int2str(misindex),''/'',int2str(misnum)]); set(u4,''String'', [''batch '',int2str(mistakePlaces{r,s}(misindex,1)),'' video '',int2str(mistakePlaces{r,s}(misindex,2))]); PlayLPMotAndCorrelations(mistakePlaces{r,s}(misindex,:)); ', i, j, 5*(i-1)+j));
    end
end
% Next
u0=uicontrol('Parent', h, 'Position', [10 140 60 40], 'FontSize', 12, 'BackgroundColor', [0.5 0.9 0.5], 'String', 'Next >', 'Callback', 'misindex=mod(misindex, misnum)+1; set(u3,''String'', [int2str(misindex),''/'',int2str(misnum)]); set(u4,''String'', [''batch'',int2str(mistakePlaces{r,s}(misindex,1)),'' video '',int2str(mistakePlaces{r,s}(misindex,2))]); PlayLPMotAndCorrelations(mistakePlaces{r,s}(misindex,:));');
% Prev
u1=uicontrol('Parent', h, 'Position', [10 180 60 40], 'FontSize', 12, 'BackgroundColor', [0.5 0.9 0.5], 'String', '< Prev', 'Callback', 'misindex=mod(misindex+misnum-2, misnum)+1; set(u3,''String'', [int2str(misindex),''/'',int2str(misnum)]); set(u4,''String'', [''batch'',int2str(mistakePlaces{r,s}(misindex,1)),'' video '',int2str(mistakePlaces{r,s}(misindex,2))]); PlayLPMotAndCorrelations(mistakePlaces{r,s}(misindex,:));');
% Play
u2=uicontrol('Parent', h, 'Position', [10 220 60 40], 'FontSize', 12, 'BackgroundColor', [0.5 0.9 0.5], 'String', 'Play', 'Callback', 'PlayLPMotAndCorrelations(mistakePlaces{r,s}(misindex,:));');
u3=uicontrol('Parent', h, 'Position', [0 0 100 20], 'Style', 'Text', 'String', '');
u4=uicontrol('Parent', h, 'Position', [100 0 150 20], 'Style', 'Text', 'String', '');

