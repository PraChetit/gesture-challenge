function res = video_body_parts(M, param, movie_type)

len = length(M);
res = [];
for i=1:len
    temp = detect_hand(M(i).cdata, M(1).cdata);
    temp = temp.position;
    if isempty(temp), temp = [-1 -1 -1]; end
    res(i,:) = temp(:);
end