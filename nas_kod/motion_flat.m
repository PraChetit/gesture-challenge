function L = motion_flat(M, param, movie_type)
    
    len = length(M);
    
    x = 240 / param(1);
    y = 320 / param(1);
    z = param(2) - 1;
    A = [eye(x - z), zeros(x - z, z)];
    B = [eye(y - z), zeros(y - z, z)];
    for i = 1:z
        A = A + [zeros(x-z, i), eye(x-z), zeros(x-z, z-i)];
        B = B + [zeros(y-z, i), eye(y-z), zeros(y-z, z-i)];
    end
    A = kron(A, ones(1, param(1)));
    B = kron(B, ones(1, param(1)))';
    
    L = zeros(len, (x-z)*(y-z));
    
    for i = 1:len
        M(i).cdata = rgb2gray(M(i).cdata);
    end
    
    Max = M(1).cdata;
    for i = 2:len
        Max = max(Max, M(i).cdata);
    end
    
    for i=1:len
        D = Max - M(i).cdata;
%         D = medfilt2(D, [7 7], 'symmetric');
        D = double(255-D);
        D(D<10) = 0;
        C = A * D * B;
        L(i, :) = C(:) / sum(C(:));
    end
end