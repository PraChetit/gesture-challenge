function dist = surface_distance(u, uone, v, vone, param)

%% mean
% unum = sum(uone, 2);
% vnum = sum(vone, 2);
% unum = unum * ones(1, length(vnum));
% vnum = vnum * ones(1, length(unum));
% vnum = vnum';
% 
% u2v = u * vone';
% v2u = uone * v';
% 
% dist = u2v ./ vnum + v2u ./ unum;
% dist = -dist;

%% max
lenu = size(u, 1);
lenv = size(v, 1);
dist = zeros(lenu, lenv);
for i=1:lenu
    temp = v(:, uone(i, :))';
    if ~isempty(temp)
        dist(i,:) = dist(i,:) + max(temp);
    end
end
for i=1:lenv
    temp = u(:, vone(i, :));
    if ~isempty(temp)
        dist(:,i) = dist(:,i) + max(temp, [], 2);
    end
end
dist = -dist;

end
