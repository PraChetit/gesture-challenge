function [correlations] = ComputeFrameCorrelations(RPK, movieK)
    % computes correlations of RP and frames in video
    % RP - resting position
    % movie - movie
    
    RPK = double(RPK);    
    
    sizeK = [];
    
    if ~all(size(RPK(:,:,1)) == size(movieK(1).cdata(:,:,1)))
        sizeK = size(RPK(:,:,1));
    end
    
%     RPK = RPK + double(bgremove(movieK(1).cdata(:,:,1)));
%     RPK = RPK ./ 2;
    correlations = zeros(length(movieK), 1);
    for i = 1:length(movieK)
        temp = movieK(i).cdata(:,:,1);
        if ~isempty(sizeK)
            temp = imresize(temp, sizeK);
        end            
        c = corrcoef(RPK(:,:,1), double(bgremove(temp)));
        correlations(i) = c(1, 2);    
    end
    
    correlations = smooth(correlations, 3);
    
    % scale it to <0,1>
    correlations = correlations - min(correlations(:)) * ones(length(correlations), 1);
    correlations = correlations ./ max(correlations(:));
    
end