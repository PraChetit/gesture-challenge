function [peaks, added, removed] = FindMostRelevantPeaks(data, n, avgLen, hard_n)
    % computes n most relevant peaks for data provided
    % data - Nx1 double - correlations with RP
    % n - # of getures to find, therefore we are looking n+1 peaks
    % angLen - estimated average length of gesture
    % hard_n - if true, won't change the # of gestures (n)
    
    if nargin < 4
        hard_n = false;
    end
    
    len = length(data);
    widthMax = ones(len, 3);
    % for each peak compute how wide peak it is
    for i = 1:len
        k = 1;
        while true
            if i - k > 0
                if data(i) < data(i-k); break; end
            end
            if i + k < len + 1
                if data(i) < data(i+k); break; end
            end
            k = k + 1;
            if (i - k < 0) && (i + k > len + 1); break; end
        end
        widthMax(i, :) = [i, k, data(i)];
    end
    
    % sort the points according to its widths
    widthMax = -sortrows(-widthMax, 2);
    
    % get the result positions
    peaks = zeros(n, 1);
    for i = 1:n+1
        peaks(i) = widthMax(i, 1);
    end
    % widthMax(1:n+3, :);
    
    added=[];
    removed=[];
    
    if hard_n; return; end
    
    % now I try to develop different models for different n
    switch n
        case 1
            if AddOnePeak(widthMax, n+2, data, avgLen)
                peaks(n+2) = widthMax(n+2);
                added(end+1) = widthMax(n+2);
            end
            return;
        case 2
            if AddOnePeak(widthMax, n+2, data, avgLen)
                peaks(n+2) = widthMax(n+2);
                added(end+1) = widthMax(n+2);
            end
            return;
        case 3
            if AddOnePeak(widthMax, n+2, data, avgLen)
                peaks(n+2) = widthMax(n+2);
                added(end+1) = widthMax(n+2);
                if AddOnePeak(widthMax, n+3, data, avgLen)
                    peaks(n+3) = widthMax(n+3);
                    added(end+1) = widthMax(n+3);
                end
            else
                k = RemoveOnePeak(widthMax, n+1, data, avgLen);
                if k ~= 0
                    removed(end+1) = peaks(k);
                    peaks(k) = [];
                end
            end
            return;
        case 4
            if AddOnePeak(widthMax, n+2, data, avgLen)
                peaks(n+2) = widthMax(n+2);
                added(end+1) = widthMax(n+2);
            else
                k = RemoveOnePeak(widthMax, n+1, data, avgLen);
                if k ~= 0
                    removed(end+1) = peaks(k);
                    peaks(k) = [];
                end
            end
            return;
        otherwise
            k = RemoveOnePeak(widthMax, n+1, data, avgLen);
            if k ~= 0
                removed(end+1) = peaks(k);
                peaks(k) = [];
            end
            return;
    end
    
    
end




