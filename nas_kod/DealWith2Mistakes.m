function dif = DealWith2Mistakes(m1, m2)

M1 = cell(5,1);
M2 = cell(5,1);
dif = cell(5);

for i=1:5
    for j=1:5
        M1{i} = [M1{i}; [m1{j,i}, j*ones(size(m1{j,i},1),1)]];
        M2{i} = [M2{i}; [m2{j,i}, j*ones(size(m2{j,i},1),1)]];
    end
end

for i=1:5
    for j=1:5
        dif{i,j} = intersect(M1{i}, M2{j}, 'rows');
    end
end

s=0; for i=1:5, for j=1:5, s=s+size(dif{i,j},1); end; end
display(s/47);