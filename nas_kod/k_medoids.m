function [med_idx, clusters] = k_medoids(D, k)

Dt = D;
[~, c] = max(sum(Dt));
med_idx = zeros(k, 1) + c;
new_idx = med_idx;
Dt(:, c) = 0;
for i = 1:k
    [~, med_idx(i)] = max(sum(Dt(unique(med_idx), :), 1));
    Dt(:, med_idx(i)) = 0;
end

change = true;
while change
    [~, clusters] = min(D(med_idx, :));
    for i = 1:k
        t = find(clusters == i);
        [~, new_idx(i)] = min(sum(D(t, t)));
        new_idx(i) = t(new_idx(i));
    end
    change = ~all(med_idx == new_idx);
    med_idx = new_idx;
end

clusters = clusters';

end