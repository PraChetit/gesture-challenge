function res = video_centroid(M, param, movie_type)

len = length(M);
res = zeros(len, 3*param*param);
x = zeros(param);
y = zeros(param);
z = zeros(param);
for i=1:len
    [temp back] = bgremove(medfilt2(double(bgremove(M(i).cdata(:,:,1))),[3 3]));
    [r c] = size(temp);
    r = r/param;
    c = c/param;
    for j=1:param
        for k=1:param
            fg = temp(r*(j-1)+1:r*j, c*(k-1)+1:c*k) ~= back;
            s = sum(sum(fg));
            if s~=0
                x(j,k) = sum(fg * (1:c)')/s;
                y(j,k) = sum((1:r) * fg)/s;
                z(j,k) = mean(temp(fg));
            else
                x(j,k) = (r+1)/2;
                y(j,k) = (c+1)/2;
                z(j,k) = 128;
            end
        end
    end
    res(i,:) = [x(:)' y(:)' z(:)'];
end