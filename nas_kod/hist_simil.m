function d = hist_simil(h1, h2, kernel, params)
    
    d = zeros(size(h1, 1), size(h2,1));
    switch kernel
        case 'MDPA' % Minimum Distance of Pairwise Assignment
            d = -MDPA(h1, h2);
        case 'chi_square' % chi-square distance
            A = eye(size(h1,2));
            A = sparse(A);
            d = -(quadratic_chi(h1, h2, A, 0.5)).^2;
            d = d/2;
        case 'HIST' % Historgam Intersection Kernel
            d = HIST(h1, h2);
        case 'riemannian' % Riemann metric
            d = -riemannian(h1, h2);
        case 'quadratic_chi'
            gauss = fspecial('gaussian', 3, 0.56);
            gauss2 = fspecial('gaussian', 3, 0.56);
            % h, w - height, width of histogram image
            % h = 240/resize_param, w = 320/resize_param
            % h*w = number of cells
            h = params(1); w = params(2);
            B = diag(ones(1,h)) + 2*(diag(ones(1, h-1), 1) + diag(ones(1, h-1), -1));
            C = diag(ones(1,w)) + 2*(diag(ones(1, w-1), 1) + diag(ones(1, w-1), -1));
            D = kron(C, B);
            D(D == 1) = gauss2(5); D(D == 2) = gauss2(2); D(D == 4) = gauss2(1);
            % D - matrix of subhistogram similarities
            % p - prepro_param
            p = params(3);
            A = imfilter( eye( p ), gauss, 'circular');
            A = kron(D, A);
            A = sparse(A);
            d = -quadratic_chi(h1, h2, A, 0.5).^2; % is squaring good idea?
        otherwise
            return;     
    end
    
end

function d = MDPA(h1, h2)
    
    n = size(h1, 2);
    mat = triu(ones(n));
    
    h1 = h1 * mat;
    h2 = h2 * mat;
    
    h1 = reshape(kron(ones(1, size(h2, 1)), h1), size(h1, 1), size(h1, 2), size(h2, 1));
    h2 = reshape(kron(ones(1, size(h1, 1)), h2), size(h2, 1), size(h2, 2), size(h1, 1));
    h2 = permute(h2, [3 2 1]);
    
    d = sum(abs(h1 - h2), 2);
    
end

function d = chi_square(h1, h2)
    
    d = 0;
    for i = 1:length(h1)
        s = (h1(i) + h2(i));
        s(s == 0) = 1;
        d = d + ((h1(i) - h2(i))^2) / s;
    end
    
end

function d = HIST(h1, h2)
    
    h1 = reshape(kron(ones(1, size(h2, 1)), h1), size(h1, 1), size(h1, 2), size(h2, 1));
    h2 = reshape(kron(ones(1, size(h1, 1)), h2), size(h2, 1), size(h2, 2), size(h1, 1));
    h2 = permute(h2, [3 2 1]);
    
    d = sum( min(h1, h2), 2 );
    
end

function d = riemannian(h1, h2)
    
    h1 = sqrt(h1);
    h2 = sqrt(h2);
    d = real(acos(h1 * h2'));
    
end

function d = quadratic_chi(h1, h2, A, m)
    
    d = zeros(size(h1, 1), size(h2,1));
    h1A = h1 * A;
    
    for i = 1:size(h2, 1)
        h = ones(size(h1, 1), 1) * h2(i, :);
        hA = h * A;
        
        Z = h1A + hA;
        Z(Z == 0) = 1;
        Z = Z.^m;
        D = (h1 - h) ./ Z;
        
        d(:, i) = sqrt(diag(max(D*A*D', 0)));
    end
    
end
