function [m] = ComputeFirstFrameCorrelations(n)

    my_data = GetCurrentPath();
    my_data = [my_data '\Data'];

    m = zeros(n,2);
    for k = 1:n
        if k<10
            str = ['devel0' int2str(k)];
        else
            str = ['devel' int2str(k)];
        end
        dt=sprintf('%s\\%s', my_data, str);
        D=databatch(dt, []);

        movies = [];
        for i=1:D.vocabulary_size
            movies(i).K = read_movie(sprintf('%s/%s/K_%d.avi', D.datapath, D.dataname, i));
        end

        frames = cell(1, D.vocabulary_size);
        for i=1:D.vocabulary_size
            frames{i} = double(bgremove(movies(i).K(1,1).cdata(:,:,1))); % with background removed
            % frames{i} = double(movies(i).K(1,1).cdata(:,:,1)); % without removed background
        end

        cor = zeros(D.vocabulary_size);
        for i = 1:D.vocabulary_size
            for j = i:D.vocabulary_size
                c = corrcoef(frames{i}, frames{j});
                cor(i,j) = c(1,2);  
                cor(j,i) = cor(i,j);
            end
        end
        m(k, :) = [k, min(cor(:))];
        sprintf('working... %d', k)
    end
    m = sortrows(m, 2);
    dlmwrite('LowestCorrelationsOfFirstFrames.csv', m);
end