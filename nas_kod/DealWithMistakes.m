mistakePlaces = cell(5, 5);
mistakes = zeros(5, 5);
for i = 1:480
    
    if ~isempty(partialMistakePlaces{i})
        mistakes = mistakes + partialMistakes{i};
        for j = 1:5
            for k = 1:5
                mistakePlaces{j, k} = [mistakePlaces{j, k}; partialMistakePlaces{i}{j, k}];
            end
        end
    end
    
end