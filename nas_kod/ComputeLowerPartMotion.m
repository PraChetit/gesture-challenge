function motions = ComputeLowerPartMotion(movie)
    
    % returns vector of positive real numbers, measuring the movement in
    % lower part of the video.

    motions = zeros(length(movie), 1);
    frames = cell(length(movie), 1);
    
    % remove background from images
    for i = 1:length(frames)
        frames{i} = bgremove(movie(i).cdata(:,:,1));
    end
    
    % compute the motions
    for k = 2:length(movie)
        D = frames{k} - frames{k-1};
        n = round(2*size(D,1)/3);
        motions(k) = mean(mean(D(n:end,:)));
    end
    
    for k = 2:length(movie)-1
        motions(k) = mean(motions(k-1:k+1));
    end
    
    % normalize result
    motions = (motions ./ max(motions(:))) .^ 2;
        
end