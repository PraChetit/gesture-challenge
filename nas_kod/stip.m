function stip(M)
    
    len = length(M);
    D = uint8(zeros(240, 320, len));
    for i=1:len
        D(:,:,i) = rgb2gray(M(i).cdata);
    end
    
    sigma = 0.3197;
    tau = 0.3197;
    fw = 3;
    
    x = reshape(kron(ones(1, fw), ones(fw, 1) * ((1:fw) - (fw+1)/2)), [fw fw fw]);
    y = -permute(x, [2 1 3]);
    t = permute(x, [1 3 2]);
    
    s = 0.5;
    K = 1.6;
    
    g = exp(-(x.^2+y.^2)/(2*sigma^2) -(t.^2)/(2*tau^2)) / (sqrt((2*pi)^3*sigma^4*tau^2))-(fw^(-3))...
        - exp(-(x.^2+y.^2)/(2*(sigma*K)^2) -(t.^2)/(2*(tau*K)^2)) / (sqrt((2*pi)^3*(sigma*K)^4*(tau*K)^2));
    g2 = exp(-(x.^2+y.^2)/(2*(sigma*s)^2) -(t.^2)/(2*(tau*s)^2)) / (sqrt((2*pi)^3*(sigma*s)^4*(tau*s)^2));
    
    L = imfilter(D, g, 'replicate');
    L = double(L);
    Lx = imfilter(L, [-1, 0, 1], 'replicate');
    Ly = imfilter(L, [1; 0; -1], 'replicate');
    Lt = imfilter(L, reshape([-1, 0, 1], [1, 1, 3]), 'replicate');
    
    Lxx = imfilter(Lx.^2, g2, 'replicate');
    Lyy = imfilter(Ly.^2, g2, 'replicate');
    Ltt = imfilter(Lt.^2, g2, 'replicate');
    Lxy = imfilter(Lx.*Ly, g2, 'replicate');
    Lxt = imfilter(Lx.*Lt, g2, 'replicate');
    Lyt = imfilter(Ly.*Lt, g2, 'replicate');
    
    k=0.005;
    
    H = (Lxx.*Lyy.*Ltt + Lxy.*Lyt.*Lxt + Lxt.*Lxy.*Lyt - Lxt.*Lyy.*Lxt - Lxy.*Lxy.*Ltt - Lxx.*Lyt.*Lyt)...
        - k * (Lxx + Lyy + Ltt).^3;
    
    
end