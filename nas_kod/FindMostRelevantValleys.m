function [valleys] = FindMostRelevantValleys(data, n, avgLen)
    
    len = length(data);
    widthMin = ones(len, 3);
    % for each peak compute how wide peak it is
    for i = 1:len
        k = 1;
        while true
            if i - k > 0
                if data(i) > data(i-k); break; end
            end
            if i + k < len + 1
                if data(i) > data(i+k); break; end
            end
            k = k + 1;
            if (i - k < 0) && (i + k > len + 1); break; end
        end
        widthMin(i, :) = [i, k, data(i)];
    end
    
    % sort the points according to its widths
    widthMin = -sortrows(-widthMin, 2);
    
    % get the result positions
    valleys = widthMin(1:n, 1);
    
end