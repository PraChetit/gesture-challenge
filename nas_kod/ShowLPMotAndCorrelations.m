function [] = ShowLPMotAndCorrelations(batch, video)

    N = 480; % number of batches
    if ((batch < 1) || (batch > N)); return; end % quit if wrong params
    V = 47; % number of  videos in each batch
    if ((video < 1) || (video > V)); return; end % quit if wrong params

    % path to the data
    my_data = GetCurrentPath();
    my_data = [my_data '\Data'];

    % process inputs. path is the physical path to the videos
    path = sprintf('%s\\devel%02d', my_data, batch);
    
    RPKpath = sprintf('%s\\avgFirstFrame.bmp', path);
    Lpath = sprintf('%s\\avgTrainLengthTrim.mat', path);
    
    pathK = sprintf('%s\\K_%d.avi', path, video);
    
    % read movie and RP (and avg length)
    L = load(Lpath);
    RPK = imread(RPKpath, 'bmp');
    movieK = read_movie(pathK);
    
    % compute the interesting things
    %motions = ComputeLowerPartMotion(movie);
    [i0, in] = trim_video(movieK);
    movieK = movieK(i0:in);
    correlations = ComputeFrameCorrelations(RPK, movieK);
    
    % plot the interesting things
    len = 1:length(movieK);
    %plot(len, motions);
    plot(len, correlations, 'r');
    hold on;
    
    N = min(max(1, round(length(len)/L.Ltrim)), 5);
    

    gaps = FindMostRelevantPeaks(correlations, N, L.Ltrim);
    % valleys = FindMostRelevantValleys(correlations, length(gaps) - 1, L.L);
    
    for i = 1:length(gaps)
        line([gaps(i), gaps(i)], [0, 1], 'Color', 'g', 'LineWidth', 2);
    end
    
    if false %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%batch < 21
        tempoSegmentPath = 'C:\Users\jakub\Desktop\Kaggle\Chalearn gesture challenge\! The Sample Code\Tempo_segment\tempo_segment';
        if batch < 10
            borders = load(sprintf('%s\\devel0%d.mat', tempoSegmentPath, batch));
        else
            borders = load(sprintf('%s\\devel%d.mat', tempoSegmentPath, batch));
        end
        borders = borders.saved_annotation{video};
        for i = 1:size(borders, 1)
            line([borders(i, 1) borders(i, 1)], [0 1], 'Color', 'g', 'LineWidth', 2);
            line([borders(i, 2) borders(i, 2)], [0 1], 'Color', 'r', 'LineWidth', 2);
        end
    end
        
end


