function [labels, path] = hmm_viterbi(local_scores)
    
    % find the magic constatns
    magic1 = 1;
    magic2 = 10;
    
    global_scores = zeros(size(local_scores));
    global_scores(:, 1) = local_scores(:, 1);
    back_pointers = cell(1, 2);
    back_pointers{1} = zeros(size(local_scores));
    back_pointers{2} = zeros(size(local_scores));
    
    % find all the global scores
    [n, l] = size(local_scores);
    for i = 2:l
        for j = 1:n
            if j < n
                costs = -Inf * ones(1, n);
                costs(j) = global_scores(j, i-1) + local_scores(j, i);
                costs(n) = global_scores(n, i-1) + magic1 * local_scores(j, i);
                [b, c] = max(costs(:));
            else
                tmp_scores = global_scores(:, i-1);
                tmp_scores(back_pointers{2}(:, i-1) < magic2) = -Inf;
                [b, c] = max(tmp_scores(:));
                b = b + local_scores(j, i);
            end
            global_scores(j, i) = b;
            back_pointers{1}(j, i) = c;
            if c == n
                back_pointers{2}(j, i) = 0;
            else
                back_pointers{2}(j, i) = back_pointers{2}(c, i-1) + 1;
            end
        end
    end
    
    % trace back the back pointers
    [~, c] = max(global_scores(:, end));
    path = zeros(1, l);
    path(end) = c;
    for i = l:-1:2
        c = back_pointers{1}(c, i);
        path(i-1) = c;
    end
    
    % and convert path to labels
    k = 1;
    labels(1, 1) = path(1);
    counter = 1;
    for i = 2:length(path)
        if labels(k) ~= path(i)
            k = k + 1;
            labels(k, 1) = path(i);
            labels(k-1, 2) = counter;
            counter = 0;
        end
        counter = counter + 1;
    end
    labels(k, 2) = counter;
    
    lbls = labels(:, 1);
    lbls = lbls(labels(:, 2) > 10);
    labels = lbls;
    labels(labels == n) = [];
    
end






