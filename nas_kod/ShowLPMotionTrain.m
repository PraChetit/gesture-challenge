function [] = ShowLPMotionTrain(K, batch)
    % shows motion hitory and correlations for each video in train set
    % K - 1 for the depth video, 0 for RGB
    % batch - # of batch to load

    my_data = GetCurrentPath();
    my_data = [my_data '\Data'];
    
    % set the path to right file
    if batch < 10
        path = sprintf('%s\\devel0%d\\devel0%d_train.csv', my_data, batch, batch);
        impath = sprintf('%s\\devel0%d\\avgFirstFrame.bmp', my_data, batch);
        movpath = sprintf('%s\\devel0%d\\', my_data, batch);
    else
        path = sprintf('%s\\devel%d\\devel%d_train.csv', my_data, batch, batch);
        impath = sprintf('%s\\devel%d\\avgFirstFrame.bmp', my_data, batch);
        movpath = sprintf('%s\\devel%d\\', my_data, batch);
    end
    train = read_file(path);
    RP = imread(impath, 'bmp');
    len = length(train);
    
    % compute motions
    motions = cell(len, 1);
    for i = 1:len
        motions{i} = ShowLowerPartMotion(K, batch, i);
    end
    
    % find correlations between frames and RP
    correlations = cell(len, 1);
    i0 = zeros(len, 1);
    in = zeros(len, 1);
    peakWithin = zeros(len, 1);
    for i = 1:len
        movie = read_movie(sprintf('%sK_%d.avi', movpath, i));
        correlations{i} = ComputeFrameCorrelations(RP, movie);
        [i0(i), in(i)] = trim_video(movie);
        peakWithin(i) = GetLargestPeakWithin(correlations{i}(i0(i):in(i)));
    end
    
    % choose # of rows and columns in plot
    switch len
        case 8
            r = 4; c = 2;
        case 9
            r = 3; c = 3;
        case 10:12
            r = 4; c = 3;
        case 13:15
            r = 5; c = 3;
        otherwise
            r = 4; c = 4;
    end
    
    % plot the plot
    for i = 1:len
        subplot(r, c, i);
        %plot(1:length(motions{i}),motions{i});
        plot(1:length(correlations{i}),correlations{i}, 'b');
        hold on
        plot(i0(i):in(i), correlations{i}(i0(i):in(i)), 'r', 'LineWidth', 2);
        xlabel(sprintf('%.3f', peakWithin(i)));
    end
    
end




