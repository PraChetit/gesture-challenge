function res = video_resize(M, scale, movie_type)

if nargin < 3
    movie_type = 'K';
end

if movie_type == 'M'
    len = length(M);
    res = zeros(len, 3*320*240*scale*scale);
    for i=1:len
        temp = imresize(M(i).cdata, scale);
        res(i,:) = temp(:);
    end
else
    len = length(M);
    res = zeros(len, 320*240*scale*scale);
    for i=1:len
        temp = imresize(bgremove(M(i).cdata(:,:,1)), scale);
        res(i,:) = temp(:);
    end
end