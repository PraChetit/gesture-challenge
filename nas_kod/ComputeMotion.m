function motion = ComputeMotion(K)

len = length(K);
denoise_param = [7 7];
resize_param = 0.5;
gap_param = 3;
max_depth_param = 15;

if len > gap_param+1
    M = cell(1,len);
    mot = cell(1,len - gap_param);
    motion = zeros(1, len);

    b = zeros(1, len);
    f = zeros(1, len);
    temp = zeros(1,len-gap_param);

    for i=1:len
        [M{i}, b(i), f(i)] = bgremove(K(i).cdata(:,:,1));
    end
    for i=1:len
        M{i}(M{i}(:,:,1)==b(i)) = mean(b);
    
        M{i} = medfilt2(M{i}, denoise_param);
        M{i} = imresize(M{i}, resize_param);
    end
    for i=1:len-gap_param
        mot{i} = abs(double(M{i+gap_param})-double(M{i}));
        mot{i} = min(mot{i}, max_depth_param);
        temp(i) = mean(mean(mot{i}));    
    end
    for i=1:(gap_param+1)
        motion = motion + [zeros(1,i-1), temp, zeros(1,gap_param+1-i)];
    end
    motion = motion./[1:gap_param, (gap_param+1)*ones(1,len-2*gap_param), gap_param:-1:1];

%    motion = smooth(motion, 7);

    % find second largest peak
    widthMax = ones(len, 3);
    % for each peak compute how wide peak it is
    for i = 1:len
        k = 1;
        while true
            if i - k > 0
                if motion(i) < motion(i-k); break; end
            end
            if i + k < len + 1
                if motion(i) < motion(i+k); break; end
            end
            k = k + 1;
            if (i - k < 0) && (i + k > len + 1); break; end
        end
        widthMax(i, :) = [i, k, motion(i)];
    end
    
    % sort the points wider than 4 according to its heights
    widthMax = -sortrows(-widthMax(widthMax(:,2) > 4, :), 3);
    
    if size(widthMax,1) >= 2 && false
        ma = widthMax(2, 3);
    else
        ma = max(motion);
    end
    mi = min(motion);
    motion = min( (motion - mi) / (ma - mi), 1);
else
    motion = ones(1, len);
end

% play with motion fps
% for i=1:len
%     image(K(i).cdata);
%     pause(motion(i)/10);
% end

% play
% for i=1:len-gap_param
%     
%         image(uint8(mot{i}));
%         pause(0.1);
%     
% end
% motion = (motion - min(motion))/(max(motion)-min(motion));
% movie(K(motion>0.2))
    