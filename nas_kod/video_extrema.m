function extrema = video_extrema(K, param, movie_type)

magic_const = 7;


O = zeros(2*magic_const+1);
for i=1:2*magic_const+1
    for j=1:2*magic_const+1
        dist = (i - magic_const - 1)^2 + (j - magic_const - 1)^2;
        O(i, j) = dist <= magic_const^2 && dist > 0;
    end
end
s = find(O);

len = length(K);
extrema = ones(240, 320, len);

for i = 1:len
    temp = double(K(i).cdata(:,:,1));
    for j=1:length(s)
        F = zeros(2*magic_const+1);
        F(magic_const+1, magic_const+1) = 1;
        F(s(j)) = -1;
        extrema(:,:,i) = extrema(:,:,i) .* (imfilter(temp, F, 'replicate', 'conv') > 0);
    end
end

end