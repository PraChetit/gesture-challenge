function res = video_hog(M, param, K)
    
    if nargin < 3
        K = M;
    end
    
    len = length(M);
    res = cell(1, 2);
    for i=1:len
        frame = medfilt2(double(K(i).cdata(:,:,1)), [7, 7]);
        temp = hog(frame, param(1), param(2));
        temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
%         s = sum(temp, 3);
%         for j=1:param(2)
%             temp(:,:,j) = temp(:,:,j) ./ s;
%         end
        temp = permute(temp, [3, 1, 2]);
        res{1}(i,:) = temp(:)./sum(temp(:));
        
        % hog on smaller scale
%         temp = hog(frame, param(1)/2, param(2));
%         temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
% %         s = sum(temp, 3);
% %         for j=1:param(2)
% %             temp(:,:,j) = temp(:,:,j) ./ s;
% %         end
%         temp = permute(temp, [3, 1, 2]);
%         res{3}(i,:) = temp(:)./sum(temp(:));        
    end
    
    res{2} = motion_mask(K, param(3));
    
end