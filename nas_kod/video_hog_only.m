function res = video_hog_only(M, param)
    
    len = length(M);
    res = [];
    for i=1:len
        temp = hog(medfilt2((double(rgb2gray(M(i).cdata))), [7, 7]), param(1), param(2));
        temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
%         s = sum(temp, 3);
%         for j=1:param(2)
%             temp(:,:,j) = temp(:,:,j) ./ s;
%         end
        temp = permute(temp, [3, 1, 2]);
        res(i,:) = temp(:)./sum(temp(:));
    end
    
    tmp = abs(res(2:end, :) - res(1:end-1, :));
    res = tmp;
    
end