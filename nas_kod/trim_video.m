function [ i0, in , frames, motion] = trim_video( depth )

motion = ComputeMotion( depth );

magic_const = 0.1;
len = length(motion);

% trim first and last frames with small motion
i0=1;
for k=1:len
    if motion(k)>magic_const
        i0=k;
        break;
    end
end
in=len;
for k=len:-1:1
    if motion(k)>magic_const
        in=k;
        break;
    end
end
i0(i0 < 5) = 1;
in(in > len - 4) = len;

if nargout > 2
    frames = i0:in;
    % trim long parts with small motion
    sequence = zeros(1,length(frames));
    sequence(in-i0+1) = motion(in)<magic_const;
    % find sequences with motion < magic_const
    for i=in-1:-1:i0
        if motion(i)<magic_const;
            sequence(i-i0+1) = sequence(i-i0+2)+1;
            sequence(i-i0+2) = 0;
        end
    end
    
    good_frames = true(1,length(frames));
    % leave 5 frames from each sequence longer then 5 frames
    where_seq = find(sequence > 5);
    for i=1:length(where_seq)
        good_frames(where_seq(i):(where_seq(i)+sequence(where_seq(i))-1)) = false;
        leave = min(in, round(linspace(where_seq(i), (where_seq(i)+sequence(where_seq(i))-1), 5)));
        good_frames(leave) = true;
    end
    
    frames = frames(good_frames);
end