function res = video_resize_both(M, K, scale)

    len = length(M);
    res = zeros(len, 4*320*240*scale*scale);
    for i=1:len
        temp1 = imresize(bgremove(K(i).cdata(:,:,1)), scale);
        temp2 = imresize(M(i).cdata, scale);
        res(i, :) = [temp1(:)' temp2(:)'];
    end

end