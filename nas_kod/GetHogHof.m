
my_data = GetCurrentPath();
my_data = [my_data '\Data'];

% set i for the batches you want to rock on
% for i = 1:20
%     path = sprintf('%s\\devel%02d', my_data, i);
% 
%     for j = 1:47
%         M = read_movie(sprintf('%s\\M_%d.avi', path, j));
%         K = read_movie(sprintf('%s\\K_%d.avi', path, j));
%         [~, ~, frames] = trim_video(K);
%         M = M(frames);
%         K = K(frames);
%         X = video_hog_hof(M, [40, 16, 40, 16], K);
%         Hog = X{1};
%         Hof1 = X{2};
%         save(sprintf('%s\\HOG_%d.mat', path, j), 'Hog');
%         save(sprintf('%s\\HOF1_%d.mat', path, j), 'Hof1');
% %         save(sprintf('%s\\HOF2_%d.mat', path, j), 'Hof2');
% %         save(sprintf('%s\\HOF3_%d.mat', path, j), 'Hof3');
%     end
%     
%     disp(i);
% end

for i = 1:20
    path = sprintf('%s\\valid%02d', my_data, i);

    for j = 1:47
        M = read_movie(sprintf('%s\\M_%d.avi', path, j));
        K = read_movie(sprintf('%s\\K_%d.avi', path, j));
        [~, ~, frames] = trim_video(K);
        M = M(frames);
        K = K(frames);
        X = video_hog_hof(M, [40, 16, 40, 16], K);
        Hog = X{1};
        Hof1 = X{2};
        save(sprintf('%s\\HOG_%d.mat', path, j), 'Hog');
        save(sprintf('%s\\HOF1_%d.mat', path, j), 'Hof1');
%         save(sprintf('%s\\HOF2_%d.mat', path, j), 'Hof2');
%         save(sprintf('%s\\HOF3_%d.mat', path, j), 'Hof3');
    end
    
    disp(i);
end