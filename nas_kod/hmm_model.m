function [parents, lstart, lend] = hmm_model(L)
    
    lstart = zeros(L+1, 1);
    lend = zeros(L+1, 1);
    parents = cell(L+1, 1);
    for i = 1:L
        parents{i} = [i, L+1];
    end
    parents{L+1} = 1:L+1;
    
end