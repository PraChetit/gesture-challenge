function [Hog, Hof1, Hof2, Hof3] = video_hog_hof2(param, K, M)
    
    len = length(K);
    
    for i = 1:len
        temp = hog(medfilt2(double(K(i).cdata(:,:,1)), [7, 7]), param(1), param(2));
        temp = temp(:,:,1:param(2)) + temp(:,:,param(2)+1:2*param(2)) + temp(:,:,2*param(2)+1:3*param(2))+ temp(:,:,3*param(2)+1:4*param(2));
        temp = permute(temp, [3, 1, 2]);
        Hog(i,:) = temp(:);
    end
    
    [Hof1, Hof2, Hof3] = video_hoof2(M, param(4), param(3));
    
end