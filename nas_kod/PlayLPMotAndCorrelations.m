function PlayLPMotAndCorrelations(bv)

    batch = bv(1);
    video = bv(2);

    N = 480; % number of batches
    if ((batch < 1) || (batch > N)); return; end % quit if wrong params
    V = 47; % number of  videos in each batch
    if ((video < 1) || (video > V)); return; end % quit if wrong params

    % path to the data
    my_data = GetCurrentPath();
    my_data = [my_data '\Data'];
    
    % process inputs. path is the physical path to the videos
    path = sprintf('%s\\devel%02d', my_data, batch);
    
    RPKpath = sprintf('%s\\avgFirstFrameSmall.bmp', path);
    Lpath = sprintf('%s\\avgTrainLength.mat', path);
    Ltrimpath = sprintf('%s\\avgTrainLengthTrim.mat', path);
    maxPeakpath = sprintf('%s\\maxPeakWithin.mat', path);
    
    D = databatch(path);
    data = subset(D, video);
    
    movie_before = get_X(data, 1);    
    movie_before = movie_before.K;
    [i0, in, frames, motions] = trim_video(movie_before);
    movie = movie_before(frames);
    
    % read movie and RP (and avg length)
    L = load(Lpath);
    Ltrim = load(Ltrimpath);
    maxPeak = load(maxPeakpath);
    RPK = imread(RPKpath, 'bmp');
    
    % compute the interesting things
    Mcorrelations = ComputeFrameCorrelations(RPK, movie_before);
    % motions = ComputeMotion(K);
    Kcorrelations = ComputeFrameCorrelations(RPK, movie);
    
    len = 1:length(movie_before);
    N = min(max(1, round(length(len)/L.L)), 5);
    Ntrim = min(max(1, round(length(movie)/Ltrim.Ltrim)), 5);
    
    [Kgaps, Kadded, Kremoved] = FindMostRelevantPeaks(Kcorrelations, Ntrim, Ltrim.Ltrim);
    [Mgaps, Madded, Mremoved] = FindMostRelevantPeaks(Mcorrelations, N, L.L);
    
    % motions warp
%      len(1)=0;
%      temp = motions > 0.1;
%      for i = 2:length(motions)
%          len(i) = len(i-1) + temp(i-1);
%      end

    

    
%     for i = 1:length(M),
%         % RGB movie frame
%         subplot(2,2,1);
%         image(frame2im(M(i)));
%         axis equal
%         if length(bv)>2
%             xlabel(sprintf('gestures = %d', bv(3)));
%         end
%         % depth movie frame
%         subplot(2,2,2);
%         image(frame2im(K(i)));
%         axis equal
%         if length(bv)>2
%             xlabel(sprintf('gestures = %d', bv(3)));
%         end
        % RGB correlations
        subplot(2,2,3);
        plot(1:length(Mcorrelations), Mcorrelations, 'r');
        hold on;
        plot(len, motions);
        for j = 1:length(Mgaps)
            line([Mgaps(j), Mgaps(j)], [0, 1], 'Color', 'g', 'LineWidth', 2);
        end
        for j = 1:length(Madded)
            line([Madded(j), Madded(j)], [0, 1], 'Color', 'b', 'LineWidth', 2);
        end
        for j = 1:length(Mremoved)
            line([Mremoved(j), Mremoved(j)], [0, 1], 'Color', 'r', 'LineWidth', 2);
        end
%         line([len(i), len(i)], [0, 1], 'Color', [0.8 0.8 0.8]);
        xlabel(sprintf('avglen = %d', L.L));
        hold off
        % depth correlations
        subplot(2,2,4);
        plot(1:length(Kcorrelations), Kcorrelations, 'r');
        hold on;
%         plot(len, motions);
        for j = 1:length(Kgaps)
            line([Kgaps(j), Kgaps(j)], [0, 1], 'Color', 'g', 'LineWidth', 2);
        end
        for j = 1:length(Kadded)
            line([Kadded(j), Kadded(j)], [0, 1], 'Color', 'b', 'LineWidth', 2);
        end
        for j = 1:length(Kremoved)
            line([Kremoved(j), Kremoved(j)], [0, 1], 'Color', 'r', 'LineWidth', 2);
        end
%         line([len(i), len(i)], [0, 1], 'Color', [0.8 0.8 0.8]);
        xlabel(sprintf('avglen = %d', Ltrim.Ltrim));
        hold off
%         pause(0.001);
%     end
    
    if false %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%batch < 21
        tempoSegmentPath = GetCurrentPath();
        tempoSegmentPath = [tempoSegmentPath '\Sample_code\Examples\tempo_segment'];
        borders = load(sprintf('%s\\devel%02d.mat', tempoSegmentPath, batch));
        borders = borders.saved_annotation{video};
        for i = 1:size(borders, 1)
            line([borders(i, 1) borders(i, 1)], [0 1], 'Color', 'g', 'LineWidth', 2);
            line([borders(i, 2) borders(i, 2)], [0 1], 'Color', 'r', 'LineWidth', 2);
        end
    end
    
end


