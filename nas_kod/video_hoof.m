function varargout = video_hoof( M, nbin, resize )
%video_hoof Computes HOOF

if nargin < 3       % DO NOT RESIZE
    cells = 1;
    W = ones(240, 320);
else                % RESIZE
    h = 240/resize;
    w = 320/resize;
    cells = h*w;
    W = kron(reshape(1:cells, h, w), ones(resize));
end

len = length(M);

for i = 1:len
    M(i).cdata = rgb2gray( M(i).cdata );
end

varargout = cell(1, nargout);
for j = 1:nargout
    varargout{j} = zeros(len - j, nbin * cells);
    for i = 1:len-j
        [Vx, Vy, ~] = optFlowLk( M(i).cdata, M(i+j).cdata, [] , 4, 2, 9e-5);
        
        V = atan2(Vy, Vx) + pi;
        V = round(V * nbin / (2 * pi));
        V(V == 0) = nbin;
        D = sqrt( (Vx .^ 2) + (Vx .^ 2) );
        
        t = accumarray([V(:), W(:)], D(:));
        
        if size(t, 1) < nbin
            t = [t; zeros(nbin - size(t,1), cells)];
        end
        
    % NORMALIZING
%         s = accumarray(W(:), D(:));
%         s = ones(nbin, 1) * s';
%         t(s == 0) = 1;
%         s(s == 0) = nbin;
%         varargout{j}(i, :) = t(:) ./ s(:); % /cells
        
    % JOINING 4x4
    %     B = [zeros(1, h-1); eye(h-1)] + [eye(h-1); zeros(1, h-1)];
    %     C = [zeros(1, w-1); eye(w-1)] + [eye(w-1); zeros(1, w-1)];
    %     A = sparse(kron(C, B));
    %     t = t * A;
        
        varargout{j}(i, :) = t(:);
    end
end

end