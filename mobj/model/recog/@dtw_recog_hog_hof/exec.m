function [labels, cuts] = exec(model, pat, cuts, data)
%[labels, cuts] = exec(model, pat, cuts)
% Make predictions with a dtw method.
% Inputs:
% pat  --    A pattern (movie or feature representation)
% cuts --    Temporal segmentatation.
% Returns:
% newpat and newcuts, the result of recognition and segmentation.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if nargin<3, cuts=[]; end
    
if isfield(pat, 'K')
	new_pat=pat.(model.movie_type);
else
    new_pat=pat;
end

% Eventually preprocess the movie
% if isfield(new_pat, 'cdata'),
%     new_pat=model.preprocessing(new_pat, model.prepro_param, pat.K);
%     new_pat = new_pat{1};
% end

if model.do_not_preprocess
    Xte{1} = load(sprintf('%s\\%s\\HOG_%d.mat', data.datapath, data.dataname, data.current_index));
    Xte{1} = Xte{1}.Hog;
    Xte{2} = load(sprintf('%s\\%s\\HOF1_%d.mat', data.datapath, data.dataname, data.current_index));
    Xte{2} = Xte{2}.Hof1;

%     Xte{1} = Xte{1}(2:end, :);
    Xte{1} = Xte{1} ./ ( sum(Xte{1}, 2) * ones(1, size(Xte{1}, 2)) );
    Xte{2} = Xte{2} ./ ( sum(Xte{2}, 2) * ones(1, size(Xte{2}, 2)) );
else
    M = get_X(data, k);
    [~, ~, frames] = trim_video(M.K);
    M.K = M.K(frames);
    M.M = M.M(frames);
    Xte = model.preprocessing(model.prepro_param, M.K, M.M);
end


if isempty(cuts)
    newest_pat{1} = new_pat;
    N = 1;
    labs = cell(1);
else
    newest_pat = split_pattern(new_pat, cuts);
    N = size(cuts, 1);
    labs = cell(N, 1);
end

for i=1:N

    % Create the local scores
    local_scores1 = model.similarity(model.Xhog, Xte{1}, model.simil_param, [240/model.prepro_param(1) - 2, 320/model.prepro_param(1) - 2, model.prepro_param(2)]);
    local_scores2 = model.similarity(model.Xhof, Xte{2}, model.simil_param, [240/model.prepro_param(3), 320/model.prepro_param(3), model.prepro_param(4)]);
    local_scores1 = local_scores1 / max(abs(local_scores1(:)));
    local_scores2 = local_scores2 / max(abs(local_scores2(:)));
    local_scores = 1*local_scores1 + 1*local_scores2;
    
    % Run the Viterbi algorithm
    [~, ~, ~, tsegment, labs{i}]=viterbi(local_scores, model.parents, model.local_start, model.local_end);
    
    % Remove the transition model label from the labels and determine the cuts
    transition_model_label=length(model.Y)+1;
    idxg=find(labs{i}~=transition_model_label & labs{i}>0);
    labs{i}=labs{i}(idxg);
    
    % if the labels are empty, force him to add at least one by removing
    % the resting position and putting it into voterbi again
    if isempty(labs{i})
        parents_new = cell(size(model.parents, 1) - 1, 1);
        for i = 1:length(model.parents) - 1
            parents_new{i} = model.parents{i}(model.parents{i} ~= size(model.parents, 1));
        end
        [~, ~, ~, tsegment, labs{i}] = viterbi(local_scores(1:end-1, :), parents_new, model.local_start(1:end-1), model.local_end(1:end-1));
        transition_model_label=length(model.Y)+1;
        idxg=find(labs{i}~=transition_model_label & labs{i}>0);
        labs{i}=labs{i}(idxg);
    end
    
    tempo_segment=[tsegment(1:end-1)'+1, tsegment(2:end)'];
    cuts=tempo_segment(idxg - 1, :);

end
labels = [];
for i=1:N
    labels = [labels; labs{i}];
end

if model.use_as_prepro
    labels=new_pat;
elseif model.use_as_segmenter
    labels=pat;
end

