function [resu, model]=train(model, data)
%[data, model]=train(model, data)
% DTW recognizer training method.
% Inputs:
% model     -- A recognizer object.
% data      -- A structure created by databatch.
%
% Returns:
% model     -- The trained model.
% resu      -- A new data structure containing the results.

% Isabelle Guyon -- isabelle@clopinet.com -- May 2012

if model.verbosity>0, fprintf('\n==TR> Training %s for movie type %s... ', class(model), model.movie_type); end

Ntr=length(data);
L=zeros(Ntr, 1);

% Reorder the templates so we don't have to worry
y=get_Y(data, 'all');
[s, idx]=sort([y{:}]);
model.Y=1:length(y);

% Preprocess data, and trim the training examples
rest_position = [];
for i=1:Ntr
    k=idx(i);
    if model.do_not_preprocess
        T{1} = load(sprintf('%s\\%s\\HOG_%d.mat', data.datapath, data.dataname, k));
        T{1} = T{1}.Hog;
        T{2} = load(sprintf('%s\\%s\\HOF1_%d.mat', data.datapath, data.dataname, k));
        T{2} = T{2}.Hof1;
        
%         T{1} = T{1}(2:end, :);
        T{1} = T{1} ./ ( sum(T{1}, 2) * ones(1, size(T{1}, 2)) );
        T{2} = T{2} ./ ( sum(T{2}, 2) * ones(1, size(T{2}, 2)) );
    else
        goto(data, k);
        [~, ~, frames] = trim_video(data.current_movie.K);
        T = model.preprocessing(data.current_movie.M(frames), model.prepro_param, data.current_movie.K(frames));
    end
    rest_position{1}(i,:)=T{1}(1,:);
    rest_position{2}(i,:)=T{2}(1,:);
    
    model.Xhog = [model.Xhog; T{1}];
    model.Xhof = [model.Xhof; T{2}];
    model.L(i)=size(T{1},1);
end
% Add the rest position
model.Xhog = [model.Xhog; mean(rest_position{1})];
model.Xhof = [model.Xhof; mean(rest_position{2})];

% Create the connectivity graph, with a transition model
[model.parents, model.local_start, model.local_end] = simple_forward_model(model.L, 1);

% Eve\ntually  test the model
if model.test_on_training_data
    resu=test(model, data);
else
    resu=result(data); % Just make a copy
end

if model.verbosity>0, fprintf('\n==TR> Done training %s for movie type %s...\n', class(model), model.movie_type); end


