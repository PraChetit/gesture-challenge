function [resu, model]=train(model, data)
%[data, model]=train(model, data)
% DTW recognizer training method.
% Inputs:
% model     -- A recognizer object.
% data      -- A structure created by databatch.
%
% Returns:
% model     -- The trained model.
% resu      -- A new data structure containing the results.

% Isabelle Guyon -- isabelle@clopinet.com -- May 2012

if model.verbosity>0, fprintf('\n==TR> Training %s for movie type %s... ', class(model), model.movie_type); end

if isa(data, 'result'),
    do_not_preprocess=1;
else
    do_not_preprocess=0;
end

Ntr=length(data);
L=zeros(Ntr, 1);

% Reorder the templates so we don't have to worry
y=get_Y(data, 'all');
[s, idx]=sort([y{:}]);
model.Y=1:length(y);

% Preprocess data, and trim the training examples
rest_position = struct('cdata',[],'colormap',[]);
for i=1:Ntr
    k=idx(i);
    if do_not_preprocess
        T=get_X(data, k);
    else
        goto(data, k);
        if strcmp(model.movie_type, 'K')
            M=data.current_movie.K;
        else
            M=data.current_movie.M;
        end
        [~, ~, frames] = trim_video(M);
        rest_position(i) = M(1);
        M = M(frames);
        [T Tone] = model.preprocessing(M, model.prepro_param);
    end
    model.X = [model.X; T];
    model.Xone = [model.Xone; Tone];
    model.L(i)=size(T,1);
end
% Add the rest position - the best of first frames
[T Tone] = model.preprocessing(rest_position, model.prepro_param);
scores = model.similarity(T, Tone, T, Tone, model.simil_param);
scores = sum(scores);
[~, best] = max(scores);
model.X=[model.X; T(best(1),:)];
model.Xone=[model.Xone; Tone(best(1),:)];

% Create the connectivity graph, with a transition model
[model.parents, model.local_start, model.local_end]=simple_forward_model(model.L, 1);

% Eventually  test the model
if model.test_on_training_data
    resu=test(model, data);
else
    resu=result(data); % Just make a copy
end

if model.verbosity>0, fprintf('\n==TR> Done training %s for movie type %s...\n', class(model), model.movie_type); end


