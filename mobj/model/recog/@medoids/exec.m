function [labels, cuts] = exec(model, pattern, cuts)
%[preprocessed_pattern, preprocessed_cuts] = exec(this, pattern, cuts)
% Executes the preprocessing for a single pattern.
% Inputs:
% this --           A preprocessing object.
% pattern --        A pattern obtained with get_X(data). In this example, the
%                   pattern is a movie.
% cuts --           Optionally give movie temporal segmentation into gestures.
% [we need this extra parameter for compatibility with other exec methods]
% Returns:
% pattern --        No preprocessing, the data are unchanged.
% cuts    --        Optionally, the pattern temporal segmentation.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if nargin<3, cuts=[]; end

new_pat = model.preprocessing(pattern.K, model.prepro_param);
Xb = split_pattern(new_pat, cuts);
N = size(cuts, 1);

labels = ones(N, 1);

for i = 1:N
    local_scores = model.similarity(model.med, Xb{i}, model.simil_param, model.hist_size);
    
    [~, cl] = max(local_scores);
%     local_scores((0:size(local_scores, 1):numel(local_scores)-size(local_scores, 1)) + cl) = -inf;
%     [~, cl2] = max(local_scores);
    Xte = zeros(1, size(model.X, 2));
    Xte(unique(cl)) = 1;
%     Xte(unique(cl2)) = 1;
    Xte(model.N > 1) = 0;
    
    Xte = ones(size(model.X, 1), 1) * Xte;
    
    scores = sum((model.X - Xte).^2, 2);
    scores = scores ./ (sum(model.X, 2) + 1);
    
    [~, labels(i)] = min(scores);
end
