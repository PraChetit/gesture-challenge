function [resu, model]=train(model, data)

Ntr = length(data);
L = zeros(Ntr, 1);
model.L = zeros(Ntr, 1);
Xtr = [];
M = [];

% Reorder the templates so we don't have to worry
y = get_Y(data, 'all');
[~, idx] = sort([y{:}]);

for i = 1:Ntr
    k = idx(i);
    goto(data, k);
    M = [M, data.current_movie.M];    
    if model.do_not_preprocess
        T = load(sprintf('%s\\%s\\HOG_%d.mat', data.datapath, data.dataname, k));
        T = T.Hog;
        T = T ./ ( sum(T, 2) * ones(1, size(T, 2)) );
    else
%         goto(data, k);
        T = model.preprocessing(data.current_movie.M, model.prepro_param);
    end
    Xtr = [Xtr; T];
    L(i)=size(T,1);
end

D = sqrt(-model.similarity(Xtr, Xtr, model.simil_param, model.hist_size));

[med, cl] = k_medoids(D, round(size(D, 1) / model.med_param));

% M = [];
% for i=1:Ntr
%     k=idx(i);
%     goto(data, k);
%     M = [M, data.current_movie.M];
% end
% N = M(med(cl));
% k = 1;
% for i = 1:Ntr
%     for j = 1:L(i)
%         subplot(1,2,1);
%         imshow(M(k).cdata);
%         subplot(1,2,2);
%         imshow(N(k).cdata);
%         k = k+1;
%         pause(0.05);
%     end
%     pause(1);
% end

model.M = M(med);

model.N = zeros(max(cl), 1);
model.med = Xtr(med, :);

model.X = zeros(Ntr, max(cl));

start = 1;
for i = 1:Ntr
    T = cl(start:start+L(i)-1);
    not_same = [true; T(2:end) ~= T(1:end-1)];
    
    model.N(unique(T)) = model.N(unique(T)) + 1;
%     model.X = [model.X; T(not_same)];
    model.X(i, unique(T)) = 1;
    model.L(i) = sum(not_same);
    
    start = start + L(i);
end

model.X(:, model.N > 1) = 0;

% [model.parents, model.local_start, model.local_end] = simple_forward_model(model.L, 0);

if model.test_on_training_data
    resu = test(model, data);
else
    resu = result(data);
end