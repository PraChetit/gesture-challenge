function resu = test(model, data)

% Here we don't touch the data
resu=result(data);

Nte = length(data);

for k = 1:Nte
    M = get_X(data, k);
    if model.do_not_preprocess
        Xte = load(sprintf('%s\\%s\\HOG_%d.mat', data.datapath, data.dataname, data.subidx(k)));
        Xte = Xte.Hog;
        Xte = Xte ./ ( sum(Xte, 2) * ones(1, size(Xte, 2)) );
    else
        Xte = model.preprocessing(M.M, model.prepro_param);
    end
    
    local_scores = model.similarity(model.med, Xte, model.simil_param, model.hist_size);
    
    local_scores = local_scores .* ((model.N) * ones(1, size(local_scores, 2)));
    local_scores = local_scores(model.X, :);
    
%     [~, cl] = max(local_scores);
%     not_same = [true, cl(2:end) ~= cl(1:end-1)];
%     Xte = model.X(cl(not_same), :);
%     
%     local_scores = model.similarity(model.X, Xte, model.simil_param, model.hist_size);
    
    [~, ~, ~, cuts, labels] = viterbi(local_scores, model.parents, model.local_start, model.local_end);
    
    idxg = find(labels>0);
    labels = labels(idxg);
    
    tempo_segment=[cuts(1:end-1)'+1, cuts(2:end)'];
    tempo_segment=tempo_segment(idxg - 1, :);
    
    % Set the results
    set_X(resu, k, labels);
    % Set cuts
    set_cuts(resu, k, tempo_segment);
end