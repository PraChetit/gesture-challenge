%==========================================================================
% BASIC_SEGMENT Preprocessing object             
%==========================================================================
% a=basic_segment(hyper) 
%
% This segmentation method splits the movie or time dependent feature
% representation into equal segments. The number of segments is determined
% by the total length divided by the average length of the training
% examples (which are single gestures).
%
% This is an object similar to a Spider object
% http://www.kyb.mpg.de/bs/people/spider/
%
% All preprocessing objects (called prepro) must have at least 3 methods: train, test, and exec
% [resu, trained_prepro]=train(prepro, databatch)
% resu = test(prepro, databatch)
% preprocessed_representation = exec(prepro, movie)

%Isabelle Guyon -- isabelle@clopinet.com -- June 2012

classdef medoids
	properties (SetAccess = public)
        % Here other hyperparameters of the preprocessing 
        % and trainable parameters can be declared
        preprocessing = @video_hog_only;
        prepro_param = [40 16];
        similarity = @hist_simil;
        hist_size = [4 6 16];
        simil_param = 'quadratic_chi';
        X = [];
        L = [];
        M = [];
        N = [];
        med = [];
        parents = {};
        local_start = [];
        local_end = [];
        med_param = 5;
        verbosity=0;            % Flag to turn on verbose mode for debug
        test_on_training_data=0;% Flag to turn on training data
        do_not_preprocess = 1;
    end
    methods
        %%%%%%%%%%%%%%%%%%%
        %%% CONSTRUCTOR %%%
        %%%%%%%%%%%%%%%%%%%
        function a = medoids(hyper) 
            % Evaluate hyper-parameters entered with the syntax of the
            % Spider http://www.kyb.mpg.de/bs/people/spider/
            eval_hyper;
        end  
        
    end %methods
end %classdef
  

 

 
 





