function [labels, cuts] = exec(model, pat, cuts)
%[labels, cuts] = exec(model, pat, cuts)
% Make predictions with a dtw method.
% Inputs:
% pat  --    A pattern (movie or feature representation)
% cuts --    Temporal segmentatation.
% Returns:
% newpat and newcuts, the result of recognition and segmentation.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if nargin<3, cuts=[]; end
    
if isfield(pat, 'K')
	new_pat=pat.(model.movie_type);
else
    new_pat=pat;
end

% Eventually preprocess the movie
if isfield(new_pat, 'cdata'),
    new_pat=model.preprocessing(pat.M, model.prepro_param, pat.K);
end

    Xte = new_pat;
    % Create the local scores
    local_scores1 = model.similarity2(model.X, Xte{1}, model.simil_param2, ...
        [240 / model.prepro_param(1) - 2, 320 / model.prepro_param(1) - 2, model.prepro_param(2)]);
%     local_scores3 = model.similarity2(model.XHog2, Xte{3}, model.simil_param2, ...
%         [240 / (model.prepro_param(1) / 2) - 2, 320 / (model.prepro_param(1) / 2) - 2, model.prepro_param(2)]);
    % FOR MOTION_MASK
    local_scores2 = model.similarity(model.Xmot, Xte{2}, model.simil_param);
    % FOR HOF
%     local_scores2 = model.similarity(model.Xmot, Xte{2}, model.simil_param, ...
%         [240 / model.prepro_param(3), 320 / model.prepro_param(3), model.prepro_param(4)]);
    
    local_scores1 = local_scores1 / max(abs(local_scores1(:)));
    local_scores2 = local_scores2 / max(abs(local_scores2(:)));
%     local_scores3 = local_scores3 / max(abs(local_scores3(:)));
    local_scores = 1*local_scores1 + 1*local_scores2;
%     local_scores = local_scores1;
    
    sliding_scores = zeros(length(model.L), size(local_scores, 2) - 9);
    best_paths = cell(length(model.L), size(local_scores, 2) - 9);
    for j = 1:length(model.L)
        for k = 1:size(local_scores, 2) - 9
            [sliding_scores(j, k), ~, best_paths{j, k}, ~, ~] = ...
                viterbi(local_scores(model.L_start(j):model.L_start(j+1)-1, k:k+9), ...
                [], model.local_start(model.L_start(j):model.L_start(j+1)-1), ...
                model.local_end(model.L_start(j):model.L_start(j+1)-1));
        end
    end
    s = size(sliding_scores, 2);
    
%     sliding_scores = sliding_scores ./ max(abs(sliding_scores(:)));
%     imshow(-sliding_scores);
    
    N = size(cuts, 1);
    labels = ones(N, 1);
    
for i=1:N
    
    % Run the Viterbi algorithm
    cut = cuts(i, :);
    if cut(2) - cut(1) < 9
%         [~, ~, ~, ~, lll]=viterbi(local_scores(:, cut(1):cut(2)), model.parents, model.local_start, model.local_end);
%         labels(i) = lll(1);
        labels(i) = 1;
    else
        start = max(1, cut(1) - 4);
        finish = min(s, cut(2) - 4);
%         scores = sum(sliding_scores(:, start:finish), 2);
        h = fspecial('gaussian', [finish - start + 1, 1], (finish - start) / 2);
%         h = abs(sum(sliding_scores(:, start:finish))); h = h';
        
        % PROBABLY A BAD IDEA WITH PERCENTAGES
%         percentages = zeros(length(model.L), 1);
%         Ls = cell(length(model.L), 1);
%         for j = 1:length(model.L)
%             Ls{j} = zeros(model.L(j), 1);
%             for k = start:finish
%                 Ls{j}(best_paths{j, k}(1, 1) : best_paths{j, k}(1, end)) = 1;
%             end
%         end
%         for j = 1:length(Ls)
%             percentages(j) = sum(Ls{j}) / model.L(j);
%         end
        
        scores = sliding_scores(:, start:finish) * h; 
%         percentages = 1 - 1 ./ (exp(-8*(percentages-0.5)) + 1) + 0.18;
%         scores = scores .* percentages;
        [~, labels(i)] = max(scores);
    end
        
end

if model.use_as_prepro
    labels=new_pat;
elseif model.use_as_segmenter
    labels=pat;
end

