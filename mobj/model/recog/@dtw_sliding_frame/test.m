function [resu, prepro] = test(model, data)
%[resu, prepro] = test(model, data)
% Make predictions with a dtw method.
% Inputs:
% model --  A recog_template object.
% data --   A data structure.
% Returns:
% resu --   The result structure. WARNING: this follows the convention of
% Spider http://www.kyb.mpg.de/bs/people/spider/ *** The result is in resu.X!!!! ***
% resu.Y are the target values.
% prepro -- The intermediate preprocessing results (held in a similar
% result stucture).
% If use_as_prepro=1, resu=prepro.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if model.verbosity>0, fprintf('\n==TE> Testing %s for movie type %s... ', class(model), model.movie_type); end

if isa(data, 'result'),
    do_not_preprocess=1;
else
    do_not_preprocess=0;
end

if model.use_as_segmenter
    resu=data;
else
    resu=result(data);
end
if nargin>1
    prepro=result(data);
end

Ntr=length(model.Y);
Nte=length(data);

% Loop over the samples 
for k=1:Nte
    if model.verbosity>0,
        if ~mod(k, round(Nte/10))
            fprintf('%d%% ', round(k/Nte*100));
        end
    end
    
    % Preprocess the movie
    M = get_X(data, k);
    [~, ~, frames] = trim_video(M.K);
    M.K = M.K(frames);
    M.M = M.M(frames);
    
    if length(M.K) <= 9
        set_X(resu, k, 1);
        continue;
    end
    
    if do_not_preprocess
        Xte = M;
    else
        Xte = model.preprocessing(M.(model.movie_type), model.prepro_param, M.K);
    end
    
    % Create the local scores
    local_scores1 = model.similarity2(model.X, Xte{1}, model.simil_param2, ...
        [240 / model.prepro_param(1) - 2, 320 / model.prepro_param(1) - 2, model.prepro_param(2)]);
    local_scores2 = model.similarity(model.Xmot, Xte{2}, model.simil_param);
    %     local_scores1 = local_scores1 + abs(max(local_scores1(:)));
    %     local_scores2 = local_scores2 + abs(max(local_scores2(:)));
    local_scores1 = local_scores1 / max(abs(local_scores1(:)));
    local_scores2 = local_scores2 / max(abs(local_scores2(:)));
    local_scores = 1*local_scores1 + 3*local_scores2;
    
    % Run the Viterbi algorithm
    sliding_scores = zeros(length(model.L), size(local_scores, 2) - 9);
    for i = 1:length(model.L)
        for j = 1:size(local_scores, 2) - 9
            [sliding_scores(i, j), ~, ~, ~, ~] = ...
                viterbi(local_scores(model.L_start(i):model.L_start(i+1)-1, j:j+9), ...
                [], model.local_start(model.L_start(i):model.L_start(i+1)-1), ...
                model.local_end(model.L_start(i):model.L_start(i+1)-1));
        end
    end
    
    sliding_scores = sliding_scores ./ max(abs(sliding_scores(:)));
    sliding_scores = sliding_scores';
    
    local_scores = any_simil(model.Xslide, sliding_scores, 'euclid2');
    
    [~, ~, ~, cuts, labels] = viterbi(local_scores, model.hmm_parents, model.hmm_local_start, model.hmm_local_end, 1);
    
%     subplot(2, 1, 1);
%     imshow(-sliding_scores);
%     subplot(2, 1, 2);
%     imshow(-sliding_scores);
%     hold on;
%     plot(path, 'Color', 'r', 'LineWidth', 2);
    
%     imshow(-sliding_scores);
    
%     plot(bp1);
%     hold on;
%     plot(bp2);
%     for i = 1:length(model.L)
%         s = sum(model.L(1:i));
%         line([1 size(local_scores, 2) - 9], [s s]);
%     end
    
    % Remove the transition model label from the labels and determine the cuts
    transition_model_label=length(model.Y)+1;
    idxg=find(labels~=transition_model_label & labels>0);
    labels=labels(idxg);
    
    % if the labels are empty, force him to add at least one by removing
    % the resting position and putting it into viterbi again
    if isempty(labels)
        parents_new = cell(size(model.hmm_parents, 1) - 1, 1);
        for i = 1:length(model.hmm_parents) - 1
            parents_new{i} = model.hmm_parents{i}(model.hmm_parents{i} ~= size(model.hmm_parents, 1));
        end
        [~, ~, ~, cuts, labels] = viterbi(local_scores(1:end-1, :), parents_new, model.hmm_local_start(1:end-1), model.hmm_local_end(1:end-1));
        transition_model_label=length(model.Y)+1;
        idxg=find(labels~=transition_model_label & labels>0);
        labels=labels(idxg);
    end
    tempo_segment=[cuts(1:end-1)'+1, cuts(2:end)'];
    tempo_segment=tempo_segment(idxg - 1, :);
    
    % Set the results
    if ~model.use_as_segmenter
        if model.use_as_prepro
            set_X(resu, k, Xte);
        else
            set_X(resu, k, labels);
        end
    end
%     set_cuts(resu, k, tempo_segment);
    
    % Save the preprocessing
    if nargout>1
        set_X(prepro, k, Xte);
%         set_cuts(prepro, k, tempo_segment);
    end
end

if model.verbosity>0, fprintf('\n==TE> Done testing %s for movie type %s... ', class(model), model.movie_type); end
