function [resu, model]=train(model, data)
%[data, model]=train(model, data)
% DTW recognizer training method.
% Inputs:
% model     -- A recognizer object.
% data      -- A structure created by databatch.
%
% Returns:
% model     -- The trained model.
% resu      -- A new data structure containing the results.

% Isabelle Guyon -- isabelle@clopinet.com -- May 2012

if model.verbosity>0, fprintf('\n==TR> Training %s for movie type %s... ', class(model), model.movie_type); end

if isa(data, 'result'),
    do_not_preprocess=1;
else
    do_not_preprocess=0;
end

Ntr=length(data);
L=zeros(Ntr, 1);

% Reorder the templates so we don't have to worry
y=get_Y(data, 'all');
[s, idx]=sort([y{:}]);
model.Y=1:length(y);

% Preprocess data, and trim the training examples
rest_position = [];
% rest_position = struct('cdata',[],'colormap',[]);
for i=1:Ntr
    k=idx(i);
    if do_not_preprocess
        T=get_X(data, k);
    else
        goto(data, k);
        if strcmp(model.movie_type, 'K')
            M=data.current_movie.K;
        else
            M=data.current_movie.M;
        end
        [~, ~, frames] = trim_video(data.current_movie.K);
%         rest_position(i) = M(1);
        M = M(frames);
        T = model.preprocessing(M, model.prepro_param, data.current_movie.K(frames));
    end
    rest_position{1}(i,:)=T{1}(1,:);
    rest_position{2}(i,:)=T{2}(1,:);
%     rest_position{3}(i,:)=T{3}(1,:);
    
    model.X = [model.X; T{1}];    
    model.Xmot = [model.Xmot; T{2}];    
%     model.XHog2 = [model.XHog2; T{3}];    
    model.L(i)=size(T{1},1);
end

A = tril(ones(length(model.L)));
model.L_start(2:length(model.L)+1) = A * model.L' + 1;
model.L_start(1) = 1;

model.X = [model.X; mean(rest_position{1})];
model.Xmot = [model.Xmot; mean(rest_position{2})];
% model.XHog2 = [model.XHog2; mean(rest_position{3})];
% Add the rest position - the best of first frames
% T = model.preprocessing(rest_position, model.prepro_param);
% scores = model.similarity(T{1}, T{1}, model.simil_param);
% scores = sum(scores);
% [~, best] = max(scores);
% model.X = [model.X; T{1}(best(1),:)];
% model.Xmot = [model.Xmot; T{2}(best(1),:)];

% Create the connectivity graph, with a transition model
[model.hmm_parents, model.hmm_local_start, model.hmm_local_end] = simple_forward_model(model.L - 9, 1);
model.hmm_parents(end) = [];
model.hmm_local_end(end) = [];
model.hmm_local_start(end) = [];
endz = find(model.hmm_local_end == 0);
endzl = length(model.hmm_local_end) + 1;
for i = 1:length(model.hmm_parents)
    if ismember(endzl, model.hmm_parents{i})
        model.hmm_parents{i} = setdiff(model.hmm_parents{i}, endzl);
        model.hmm_parents{i} = [model.hmm_parents{i}, endz'];
    end
end
model.local_start = zeros(size(model.X, 1), 1);
model.local_end = zeros(size(model.X, 1), 1);

% for z=1:Ntr
%     Xte{1} = model.X(model.L_start(z):model.L_start(z+1)-1, :);
%     Xte{2} = model.Xmot(model.L_start(z):model.L_start(z+1)-1, :);
%     
%     % Create the local scores
%     local_scores1 = model.similarity2(model.X, Xte{1}, model.simil_param2, ...
%         [240 / model.prepro_param(1) - 2, 320 / model.prepro_param(1) - 2, model.prepro_param(2)]);
%     local_scores2 = model.similarity(model.Xmot, Xte{2}, model.simil_param);
%     %     local_scores1 = local_scores1 + abs(max(local_scores1(:)));
%     %     local_scores2 = local_scores2 + abs(max(local_scores2(:)));
%     local_scores1 = local_scores1 / max(abs(local_scores1(:)));
%     local_scores2 = local_scores2 / max(abs(local_scores2(:)));
%     local_scores = 1*local_scores1 + 3*local_scores2;
%     
%     % Run the Viterbi algorithm
%     sliding_scores = zeros(length(model.L), size(local_scores, 2) - 9);
%     for i = 1:length(model.L)
%         for j = 1:size(local_scores, 2) - 9
%             [sliding_scores(i, j), ~, ~, ~, ~] = ...
%                 viterbi(local_scores(model.L_start(i):model.L_start(i+1)-1, j:j+9), ...
%                 [], model.local_start(model.L_start(i):model.L_start(i+1)-1), ...
%                 model.local_end(model.L_start(i):model.L_start(i+1)-1));
%         end
%     end
%     
%     sliding_scores = sliding_scores ./ max(abs(sliding_scores(:)));
%     sliding_scores = sliding_scores';
%     
%     model.Xslide = [model.Xslide; sliding_scores];
% end

% Eve\ntually  test the model
if model.test_on_training_data
    resu=test(model, data);
else
    resu=result(data); % Just make a copy
end

if model.verbosity>0, fprintf('\n==TR> Done training %s for movie type %s...\n', class(model), model.movie_type); end


