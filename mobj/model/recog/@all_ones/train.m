function [resu, this]=train(this, datag)
% Eventually  test the this
if this.test_on_training_data
    resu=test(this, datag);
else
    resu=result(datag); % Just make a copy
end

if this.verbosity>0, fprintf('\n==TR> Done training %s for movie type %s...\n', class(this), this.movie_type); end


