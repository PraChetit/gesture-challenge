function [labels, cuts] = exec(model, pat, cuts)
%[labels, cuts] = exec(model, pat, cuts)
% Make predictions with a PCA method.
% Inputs:
% pat  --    A pattern (movie or feature representation)
% cuts --    Temporal segmentatation.
% Returns:
% newpat and newcuts, the result of recognition and segmentation.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if nargin<3, cuts=[]; end

if isfield(pat, 'K')
    pat=pat.(model.movie_type);
end
    
% Checks whether cuts exist
if isempty(cuts)
    cuts=model.segmenter(pat, model.segment_param); 
end

% Compute the reconstruction error with the different PCA models and 
% assign the label corresponding to the gesture with lowest
% reconstruction error    
N=size(cuts,1);
labels=ones(1,N);