function [resu, this]=train(this, data)
%[data, this]=train(this, data)
% Template preprocessing training method.
% Inputs:
% this      -- A preprocessing object.
% data      -- A structure created by one of the data objects.
%
% Returns:
% this      -- The trained preprocessing object.
% resu      -- A new data structure containing the results.
% Note: 
% - data is a databatch, which is an object oriented database of movies
% reading them directly from disk (low mwmory usage).
% - resu is a data structure that includes results of preprocessing or
% recognition as a data matrix. It consumes a lot of memory.

% Isabelle Guyon -- isabelle@clopinet.com -- May 2012

if this.verbosity>0, fprintf('\n==TR> Training %s for movie type %s... ', class(this), this.movie_type); end

% Here we just compute the average movie length is trainign data to
% eventually perform a trivial temporal segmentation
Ntr=length(data);
L=zeros(Ntr, 1);

% Load the resting position
my_data = GetCurrentPath();
my_data = [my_data '\Data'];
this.RPK = imread(sprintf('%s\\%s\\avgFirstFrame%s.bmp', my_data, data.dataname, this.small), 'bmp');
this.RPK = this.RPK(:,:,1);

for k=1:Ntr
    frames = get_frames(data, k);
    L(k) = length(frames);
end
this.len = mean(L);

% Eventually test the model
resu=data;
if this.test_on_training_data
    % We cheat because we know the truth value of the segmentation for
    % training data. No need to make mistakes here.
    for k=1:Ntr
        X = get_X_frames(data, k);
        correlations = ComputeFrameCorrelations(this.RPK, X.K);
        peaks = sort(FindMostRelevantPeaks(correlations, 1, this.len, true));
        if (peaks(2) - peaks(1)) < 11
            peaks(1) = 1;
            peaks(2) = length(correlations);
        end
        set_cuts(resu, k, [peaks(1) peaks(2)]);
    end
end

if this.verbosity>0, fprintf('\n==TR> Done training %s for movie type %s...\n', class(this), this.movie_type); end


