function [pattern, cuts] = exec(this, pattern, cuts)
%[preprocessed_pattern, preprocessed_cuts] = exec(this, pattern, cuts)
% Executes the preprocessing for a single pattern.
% Inputs:
% this --           A preprocessing object.
% pattern --        A pattern obtained with get_X(data). In this example, the
%                   pattern is a movie.
% cuts --           Optionally give movie temporal segmentation into gestures.
% [we need this extra parameter for compatibility with other exec methods]
% Returns:
% pattern --        No preprocessing, the data are unchanged.
% cuts    --        Optionally, the pattern temporal segmentation.

% Isabelle Guyon -- May 2012 -- isabelle@clopinet.com

if nargin<3, cuts=[]; end

if this.verbosity>1, fprintf('\n==TE> Executing %s for movie type %s... ', class(this), this.movie_type); end

L=length(pattern.K);

N=min(max(1, round(L/this.len)), 5); % Number of gestures
if L > 8
%     [i0, in] = trim_video(pattern.K);
%     pattern.K = pattern.K(i0:in);
    correlations = ComputeFrameCorrelations(this.RPK, pattern.K);
    peaks = FindMostRelevantPeaks(correlations, N, this.len);
    peaks = sort(peaks);
    N = length(peaks) - 1;
    cuts = zeros(N,2);
    for i = 1:(length(peaks) - 1)
        cuts(i,:) = [peaks(i) + 1, peaks(i+1)]; % VYMTSLET -------------------------------------------------------------------------
    end
else
    cuts(1,:) = [2,L];
end

if this.verbosity>1, fprintf('\n==TE> Executing %s for movie type %s... ', class(this), this.movie_type); end
